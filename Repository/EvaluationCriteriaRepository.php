<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\KpiBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class EvaluationCriteriaRepository extends EntityRepository
{
    public function getEvaluationCriteria()
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.name AS reportName', 'e.reportTarget', 'e.marksTarget', 'e.type AS reportType', 'e.slug');

        $results = $qb->getQuery()->getArrayResult();

        $data = [];
        foreach ($results as $result){
            $data[$result['reportType']][] = $result;
        }

        return $data;
    }

}
